const cred=require('./credential.json')

describe('HotelCreteAndDelete', () => {
    beforeEach(() => {
        Cypress.Cookies.preserveOnce('sid', 'access_token')
      })

    it('logIn', () => {
        cy.visit('https://sanatop-test.quirco.com/')
        cy.get('#Username').type(cred.login)                //input login
        cy.get('#Password').type(cred.pass)                 //input password
        cy.get('button').contains('Login').click()
        cy.url().should('include','sanatop/private/orders') // true
      })

    it('create new hotel', () => {
      cy.get('span').contains('Санатории').click()
      cy.url().should('include', '/sanatop/admin/sanatoriums') // true
      
    
    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
      })                                                      //ignore exception

      cy.get('span').contains('Создать').click({force: true})
      cy.get('input[aria-label="Название санатория"]').type('Тестовый санаторий')
      cy.get('input[type="search"]').type('О')
      cy.get('div').contains('Омск').click()
      cy.get('input[aria-label="Код санатория"]').type('13')
      cy.get('input[aria-label="Максимальная цена"]').type('14000')
      cy.get('span').contains('Сохранить').click()
      cy.get('div[class="noty_bar noty_type__success noty_theme__mint noty_close_with_click noty_has_timeout"]').should('eq','Санаторий успешно сохранен.')
    })

    it('delete new hotel', () => {
      cy.get('span').contains('Удалить').click()
      cy.get('div[class="q-card"').contains('Удалить').click()
      cy.url().should('include', '/sanatop/admin/sanatoriums')
    })
});